
const { ObjectId } = require('mongodb');

/**
 * Класс для создания базы данных с рейтингами игр и пользователей (считай кеш)
 */
module.exports = class TopRatingGames {
    constructor(dbConnection){
        this.connect = dbConnection;

        // подключаемся к новой бд
        this.db = this.connect.db('rating');

        // получаем коллекцию пользователей РЕЙТИНГА
        this.usersCollection = this.db.collection('users');

        this.dbDataYGS = this.connect.db('dataYGS');

        // получаем коллекцию ВСЕХ пользователей
        this.allUsersCollection = this.dbDataYGS.collection('users');

        // получаем коллекцию со всеми играми пользователей
        this.userGamesCollection = this.dbDataYGS.collection('userGames');


        // получаем коллекцию с основной инфой об играх
        this.commonInfoCollection = this.dbDataYGS.collection('gamesInfo');

        // получаем коллекцию с инфой по языку
        this.langInfoCollection = this.dbDataYGS.collection('gamesInfo_ru');

    }

    /**
     * Метод для запуска работы
     */
    async work(){

        // создаем коллекции
        // await this.createUsers();

        // пишем данные
        await this.seedUsers();

        // удаляем коллекции
        // await this.deleteFakeUsers();

    }

    /**
     * Создает колеекцию в базе кеше для хранения топа пользователей
     */
    async createUsers(){

        // пишем фейковый документ
        let result = await this.usersCollection.insertOne({
            _id: new ObjectId('631f7705b9a08b4348745309'),
            userLogin: '1111',
            userPlaceRating: 12,
            userIcon: 'sfsg,lsg,rl,',
            userCountPlatinum: 235,
            userCountPoints: 2305.23,
            userCountFinish: 500,
            userProgress: -3,
            bestGame: {
                gameId: 'sdgsgsgsg',
                gameIcon: 'dfgdfgdfgdfg',
                gameRewardPoints: 120.23,
            },
            bestDLC: {
                gameId: 'sdgsgsgsg',
                gameIcon: 'dfgdfgdfgdfg',
                gameRewardPoints: 120.23,
            },
        });

        // проверяем была ли удачная запись документа
        if(!result.acknowledged){ throw new Error( 'Ошибка создания коллекции createUsers') }else{
            console.log('createUsers ==> ', result.acknowledged)
        }

        // устанавливаем индексы
        this.usersCollection.createIndex({ userCountPoints: -1 });

        console.log('Коллекция createUsers успешно создана!');

    }

    /**
     * Удаляет документ в users
     */
     async deleteFakeUsers(){
        let result = await this.usersCollection.deleteOne({userLogin : "1111"});
        if(!result.acknowledged){ throw new Error('Ошибка удаления deleteFakeUsers ==> userLogin : "1111"' ) }
        console.log('Фейковый документ успешно удален ==> fakeUsers');
    }

    async seedUsers(){
        try{
            // получаем 1000 пользователей из общей коллекции
            let cursorAllUsers = await this.allUsersCollection.find().sort({userPlaceRating: -1}).limit(200);

            let countUser = 200;

            while(await cursorAllUsers.hasNext()){

                // получаем объект пользователя из общей коллекции
                let objUser = await cursorAllUsers.next();
                // console.log('objUser._id ==> ', objUser._id); return;
                // получаем об лучшей главной игре
                let [bestGame] = await this.userGamesCollection.find({$and: [{userId: objUser._id}, {gameMainProgress: 100}]}).sort({gameMainRewardPoints: -1}).limit(1).toArray();
                // console.log('bestGame ==> ', bestGame); return;

                // проверяем есть ли совпадение по игре TODO потом удалить!!!!! ТОЛЬКО для ПРОБЫ!!!
                if(!bestGame){
                    // let result  = await this._getAnotherPlayer(countUser);
                    // console.log('reault ==> ', result); return;
                    ({countUser, objUser, bestGame} = await this._getAnotherPlayer(countUser));
                    console.log('Отсутствует bestGame!!!');
                }

                let objBestGame = await this.commonInfoCollection.findOne({_id: bestGame.gameMainId});
                // console.log('bestGame ==> ', bestGame);
                // console.log('objBestGame ==> ', objBestGame); return;
                // let bestGameLangInfo = await this.langInfoCollection.findOne({_id: bestGame.gameMainId});

                // получаем лучшее DLC 
                let [bestDLC] = await this.userGamesCollection.find({$and: [{userId: objUser._id}, {'gameList.gameProgress': 100}]}, {projection: {gameList: 1,}}).sort({'gameList.gameRewardPoints': -1}).limit(1).toArray();
                // let bestDLCLangInfo = await this.langInfoCollection.findOne({_id: objUser.userGameFinish.userBestDLC});
                let IdBestDLC;
                let max = 0;
                for(let i = 0; i < bestDLC.gameList.length; i++){
                    if(bestDLC.gameList[i].gameRewardPoints > max){
                        max = bestDLC.gameList[i].gameRewardPoints;
                        IdBestDLC = bestDLC.gameList[i].gameId;
                    }
                }
                let objBestDLC = await this.commonInfoCollection.findOne({_id: IdBestDLC});

                // console.log('objBestDLC ==> ', objBestDLC); return;
                // создаем документ и сразу записываем в коллекцию рейтинга
                let resultRatingUsers = await this.usersCollection.insertOne({
                    _id: objUser._id,
                    userLogin: objUser.userLogin,
                    userPlaceRating: objUser.userPlaceRating,
                    userIcon: objUser.userIcons.m ?? objUser.userIcons.l,
                    userCountPlatinum: objUser.userCountTrophies.platinum,
                    userCountPoints: objUser.userCountPoints,
                    userCountFinish: objUser.userGameFinish.userCountFinish,
                    userProgress: objUser.useProgress ?? 0,
                    bestGame: {
                        gameId: objUser.userGameFinish.userBestGame,  // тут пока единицы, надо запустить скрипт по поиску лучших для пользователей
                        gameIcon: objBestGame.gameInfo.ru.gameIcon,
                        gameRewardPoints: objBestGame.gameRewardPoints ?? 0,
                    },
                    bestDLC: {
                        gameId: objUser.userGameFinish.userBestDLC,  // тут пока единицы, надо запустить скрипт по поиску лучших для пользователей
                        gameIcon: objBestDLC.gameInfo.ru.gameIcon,
                        gameRewardPoints: objBestDLC.gameRewardPoints ?? 0,
                    },
                });

                // проверяем результат
                if (!resultRatingUsers.acknowledged) { 
                    throw new Error('Запись не удалась => seedUsers');
                }

                ++countUser;
            }


        }catch(err){
            console.log('Ошибка в seedUsers ==> ', err);
        }

        console.log('Запись пользователей в Rating users прошла успешно!');
    }

    /**
     * ВРЕМЕННЫЙ метод для получения польщователя в топ. Так как пока не все игры пользователей перенесены. УДАЛИТЬ!!!!!!!!!!
     * @param {Number} skip - кол-во элементов для пропуска
     * @returns 
     */
    async _getAnotherPlayer(skip){
        let objUser; // = await this.allUsersCollection.find().sort({userPlaceRating: -1}).skip(skip).limit(1);
        let bestGame; //await this.userGamesCollection.find({$and: [{userId: objUser._id}, {gameMainProgress: 100}]}).sort({gameMainRewardPoints: -1}).limit(1).toArray();
        while(!bestGame){
            objUser = await this.allUsersCollection.find().sort({userPlaceRating: -1}).skip(skip).limit(1).next();
            // console.log('objUser ==> ', objUser); return
            [bestGame] = await this.userGamesCollection.find({$and: [{userId: objUser._id}, {gameMainProgress: 100}]}).sort({gameMainRewardPoints: -1}).limit(1).toArray();
            console.log(`_getAnotherPlayer ${skip} `);
            
            ++skip;
        }
        return {
            objUser,
            bestGame,
            countUser: skip,
        }
    }
}

