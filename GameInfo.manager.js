
// класс для создания идентификаторов документа, для запроса в mongodb
const { ObjectId } = require('mongodb');

/**
 * Класс для получения информации по играм. В том числе информации на разных языках.
 */
module.exports = class GameInfo {
    constructor(db, managers){

        // связь с другими менеджерами
        this.managers = managers;

        // коллекция с общими данными по играм, незавизящими от языка польщователя
        this.commonInfo = db.collection('gamesInfo');

        // колеекция с трофеями
        this.trophies = db.collection('trophies');

        // коллекции с данными для разных языков
        this.langsCollections = {
            ru: db.collection('info_ru'),
        }

    }

    /**
     * Формирует и возвращает список(объект) полей которые необходимо вернуть из базы данных
     * @param {Object<Array>} inputFields - массив объектов с информацией о запрашиваемых полях
     * @returns {Object} объект со списком полей которые необходимо запросить в бд
     */
    _getFields(inputFields){
        let needFields = {};
        for(let i = 0; i < inputFields.length; i++){
            needFields[inputFields[i].name.value] = 1;
            // console.log('inputFields[i] ==> ', inputFields[i]);
        }
        return needFields;
    }

    /**
     * Возвращает объект с общими данными по игре. Независящими от языка пользователя.
     * Для запроса данных по конкретному языку, передает методу GameInfo.getlangInfo(вызывается автоматичечки) параметры через общий объект context, устанавлявая в context  поле gameInfo.lang равным args.lang. 
     * @param {Object} parentData -  Объект с данными от родительского запроса. 
     * @param {Object} args - ОБЯЗАТЕЛЬНО должен содержать поле _id - идентификатор игры, поле lang - язык пользователя. Объект с аргументами запроса.
     * @param {Object} context - Объект, совместно используемый всеми преобразователями(резолверами). 
     * @param {Object} info - Объект с информацией об операции graphql. В данном случае - запрос(query)
     * @returns {Object} - объект с данными по игре
     */
    async getGameInfo(parentData, args, context, info){
        // TODO проверить входящие параметры
        // console.log('getGameInfo ==> ', args);
        try{
            // устанавливаем общие аргументы, для остальных резолверов
            // context.gameInfo = {lang: args.lang};
            // получаем имена необходимых полей
            let needFields = this._getFields(info.fieldNodes[0].selectionSet.selections);
            needFields = Object.assign(needFields, {gameParent: 1});
            
            // запрашиваем данные с проекцией только необходимых полей
            let result = await this.commonInfo.findOne({_id: new ObjectId( args.id )}, {projection: Object.assign(needFields, {gameCommunicationId: 1})});


            // TODO это временное решение для демонстрации
            if(!result.gameCommunicationId){
                let mainGame = await this.commonInfo.findOne({_id: result.gameParent}, {projection: {gameCommunicationId: 1}});
                result.gameCommunicationId = mainGame.gameCommunicationId;
            }
            result.gameInfo = result.gameInfo[args.lang];

            // TODO потом удалить это!!!  9.9974418951338
            result.gameDifficulty = parseFloat((result.gameDifficulty + '').slice(0, 3));
            result.gameImpression = parseFloat((result.gameImpression + '').slice(0, 3));
            result.gameTime = parseFloat((result.gameTime + '').slice(0, 3));
            result.gameRewardPoints = Math.trunc(result.gameRewardPoints);
            // TODO вот до этого
            // console.log('result getGameInfo ==> ', result);
            return result;
            // let infoprofile = await this.commonInfo.findOne({_id: new ObjectId( args.id )}, {projection: needFields});
            // let infoprofile = await this.usersCollection.findOne({_id: args.id});
            // return infoprofile;
        }catch(err){
            console.log('err getGameInfo ==> ', err);
        }
    }

    /**
     * Метод запрашивает данные одной игры (gameId) с полями(needFields) и возвращает результат
     * @param {String} gameId - ОБЯЗАТЕЛЬНО строка с идентификатором игры
     * @param {Object} needFields - Объект с полями для поля projection.
     * @returns {Object | null} - объект с данными по игре
     */
    async getGameData(gameId, needFields){
        try{
            // запрашиваем данные с проекцией только необходимых полей
            return await this.commonInfo.findOne( {_id: gameId}, {projection: needFields} );
        }catch(err){
            console.log('Ошибка в методе gameInfo => getGameData ==> ', err);
        }
    }

    /**
     * Возвращает строку URL с иконкой игры
     */
    getGameIcon(parentData, args){
        // console.log('getGameIcon parentData ==> ', parentData);
        return parentData.gameIcon.m;
    }

    /**
     * Возвращает список дополнений (dlc) для главной. Возвращает _id и gameName
     * @param {Object} parentData - ОБЯЗАТЕЛЬНО должен содержать поле _id - идентификатор игры. Объект с данными от родительского запроса. 
     * @param {Object} args - Объект с аргументами запроса.
     * @param {Object} context - Объект, совместно используемый всеми преобразователями(резолверами). 
     * @param {Object} info - Объект с информацией об операции graphql. В данном случае - запрос(query)
     * @returns {Object<Array>} - возвращает массив объектов которые содержат gameName на выбранном языке и _id
     */
    async getDLCList(parentData, args, context, info){
        try {
            //  проверяем на void
            if(args.void){
                return [];
            }
	        // проверяем есть ли такой язык в списке доступных
	        if( !this.langsCollections[args.lang] ){
	            //TODO бросить ошибку grqphql!!!!!
	            // пока выбрасываем жесткую ошибку
	            throw new Error( `Нет такого языка среди доступных ==> ${args.lang}` );
	        }
	        let result = [];

            // проверяем является ли игра главной
            let gameObj = await this.commonInfo.findOne({_id: parentData._id});
            if(!gameObj.gameCommunicationId){
                // тогда ищем главную 
                let mainGameObj = await this.commonInfo.findOne({_id: gameObj.gameParent});
                // console.log('mainGameObj ==> ', mainGameObj);
                result.push({
                    _id: mainGameObj._id,
                    gameName: mainGameObj.gameInfo[args.lang].gameName,
                });
                // презаписываем пришедший id(он от dlc) идентификатором который только что получили, для правильного запроса
                parentData._id = mainGameObj._id;
            }

	        // получаем массив с идентификаторами dlc
	        let cursorDLCList = await this.commonInfo.find({ gameParent:  parentData._id}, {projection:  {_id: 1, gameInfo: 1, }});
            
	        while( await cursorDLCList.hasNext() ){
	            // получаем объект с одним дополнением
	            let objDLC = await cursorDLCList.next();
	
	            // // запрашиваем данные с проекцией только необходимых полей
	            // let langObj = await this.langsCollections[args.lang].findOne( {_id: objDLC._id}, {projection: {gameName: 1}} );
	
	            // // проверяем результат
	            // if( !langObj.gameName ){
	            //     throw new Error( `Не получен gameName lang ==> ${args.lang}  идентификатор ==> ${objDLC._id}` );
	            // }
	
	            // дописываем поля с названием игры в объект с dlc
	            objDLC.gameName = objDLC.gameInfo[args.lang].gameName;
                
	            // добавляем в результат
	            result.push(objDLC);
	        }
            // console.log('result dlc ==> ', result);
	        return result;
        } catch (err) {
            console.log('Ошибка в getDLCList  ==> ', err);
        }
    }

    /**
     * TODO предусмотреть возврат данных без передачи userId - то есть просто список трофеев. И также переписать на запрошенные поля через info.
     * Возвращает список(массив) трофеев и дату их получения пользователем
     * @param {Object} parentData - объект с данными от вышестоящего резолвера. ОБЯЗАТЕЛЬНО должен содержать _id игры
     * @param {Object} args - объект с аргументами. lang - язык пользователя. gameId - id игры. userId - id пользователя
     * @param {Object} context - Объект, совместно используемый всеми преобразователями(резолверами). 
     * @param {Object} info - Объект с информацией об операции graphql. В данном случае - запрос(query)
     * @returns 
     */
    async getTrophyList(parentData, args, context, info){
        console.log('getTrophyList ==> ', args)
        try{
            let result = [];

            // получаем весь список трофеев для игры
            let cursorTrophies = await this.trophies.find(
                { trophyGameId: ObjectId(args.gameId)}, 
                { projection:  {
                    // trophyGameId: 1, 
                    _id: 1,
                    trophyType: 1, 
                    trophyIcon: 1, 
                    trophyCountHints: 1, 
                    trophyRareYgs: 1,
                    trophyHidden: 1,
                    trophyUnreachable: 1,
                   [ `trophyDescriptions.${args.lang}`] : 1,
                }
                }
            ).sort({ trophyId: 1 });

            // проверяем запрашивается ли трофеи пользователя или просто для игры
            let earnedTrophyList;
            if(args.userId){
                // получаем список полученных пользователем трофеев
                ({ earnedTrophyList } = await this.managers.earnedTrophy.getEarnedTrophies({}, {userId: args.userId, gameId: args.gameId,}));
            }

            // крутим весь список трофеев
            while( await cursorTrophies.hasNext() ){

                // получаем один трофей из списка
                let objTrophy = await cursorTrophies.next();
                // console.log('objTrophy ==> ', objTrophy);
                // добавялем в результирующий массив
                result.push({
                    id: objTrophy._id,
                    trophyType: objTrophy.trophyType,
                    trophyName: objTrophy.trophyDescriptions[args.lang].trophyName,
                    trophyDetail: objTrophy.trophyDescriptions[args.lang].trophyDetail,
                    trophyIcon: objTrophy.trophyIcon.m,
                    trophyCountHints: objTrophy.trophyCountHints ?? 0, // TODO потом удалить проверку на null
                    trophyHidden: objTrophy.trophyHidden,
                    trophyRareYgs: objTrophy.trophyRareYgs,

                });
                // console.log('objTrophy.trophyCountHints ==> ', objTrophy.trophyCountHints);
                // проверяем на недоступность трофея
                if(objTrophy.trophyUnreachable){
                    // тогда дописываем в объект трофея соответствующее поле с инфой о причине недоступности
                    result[result.length - 1].trophyUnreachable = objTrophy.trophyUnreachable;
                }
                // проверяем запрашивается ли трофеи пользователя или просто для игры
                if(args.userId){
                    // крутим список полученных трофеев
                    for( let i = 0; i < earnedTrophyList.length; i++ ){
                        
                        // если совпадают id у получуннего трофея и у трофея из общего списка
                        
                        if( objTrophy._id.toString() === earnedTrophyList[i].earnedTrophyId.toString() ){
                            
                            // добавляем дату получения
                            result[result.length - 1].earnedDateTime = earnedTrophyList[i].earnedDateTime;
        
                            // удаляем совпадение
                            earnedTrophyList.splice(i, 1);
                        }
                    }
                }

            }

            return result
        }catch(err){
            console.log('Ошибка в getTrophyList  ==> ', err);
        }
    }

    /**
     * Возвращает один трофей и дату его получения пользователем
     * @param {Object} parentData - объект с данными от вышестоящего резолвера. ОБЯЗАТЕЛЬНО должен содержать _id игры
     * @param {Object} args - объект с аргументами. lang - язык пользователя. gameId - id игры. userId - id пользователя
     * @param {Object} context - Объект, совместно используемый всеми преобразователями(резолверами). 
     * @param {Object} info - Объект с информацией об операции graphql. В данном случае - запрос(query)
     * @returns 
     */
    async getTrophyInfo(parentData, args, context, info){
        try{
            let result = await this.trophies.findOne(
                { $and: [{trophyGameId: ObjectId(args.gameId)}, {_id: ObjectId(args.trophyId)}]}, 
                { projection:  {
                    // trophyGameId: 1, 
                    _id: 1,
                    trophyType: 1, 
                    trophyIcon: 1, 
                    trophyCountHints: 1, 
                    trophyRareYgs: 1,
                    trophyHidden: 1,
                    trophyUnreachable: 1,
                    trophyUsersEarned: 1,
                   [ `trophyDescriptions.${args.lang}`] : 1,
                }
                }
            );

            result.trophyName = result.trophyDescriptions[args.lang].trophyName;
            result.trophyDetail = result.trophyDescriptions[args.lang].trophyDetail;
            result.trophyIcon = result.trophyIcon.m;

                console.log('trophy one ==> ', result);
                return result;
        }catch(err){
            consolr.log('Ошибка в getTrophyInfo ==> ', err);
        }
    }
}
