
// получаем типы данных
const { GraphQLNonNull, GraphQLString } = require('graphql');

// основной тип игры
const GameInfo = require('../dataTypes/game/gameInfo.type.js');

// получаем менеджера для работы с данными
const  dataManager = require('../dataManager/Main.manager.js').gameInfo;

module.exports = {
    type: GameInfo,
    description: 'Получаем полную инфу по игре',
    args: {
        id: {
            type: new GraphQLNonNull( GraphQLString ),
            description: 'Идентификатор игры',
        },
        lang: {
            type: new GraphQLNonNull( GraphQLString ),
            description: 'Язык интерфейса пользователя, должен быт: ru, en',
        },
    },
    resolve: dataManager.getGameInfo.bind(dataManager),
};
