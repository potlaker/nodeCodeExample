
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList, GraphQLFloat } = require('graphql');

const GameTags = require('./gameTags.type.js');

module.exports = new GraphQLObjectType({
    name: 'TopGame',
    description: 'Одна игра в списке топовых',
    fields: () => ({

        _id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'Идентификатор игры в коллекции gamesInfo',
        },
        gameId: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'Идентификатор игры в коллекции gamesInfo',
        },
        gameName: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'Название игры',
        },
        gamePlatforms: {
            type: new GraphQLNonNull(new GraphQLList( GraphQLString )),
            description: 'Массив с названиями платформ',
        },
        gameIcon: {
            type: new GraphQLNonNull( GraphQLString ),
            description: 'Адрес иконки',
        },   
        gameTags: {
            type: new GraphQLNonNull( GameTags ),
            description: 'Теги игры',
        },

    })
})
