
const { MongoClient, ObjectId } = require('mongodb');

/** класс отвечающий за коммуникацию с базой данных */
class DBManager {
   constructor() { }

   // TODO продумать передачу параметров для различных типов аккаунтов. Одни просто для статистики
   // другие - зареганые пользователи. И работа с разными коллекциями
   /** инициализирует экземпляр */
   async init() {
      try {
         this.connect = await MongoClient.connect('mongodb://127.0.0.1:27017', {
            maxPoolSize: 20,
         });
         // подключаемся к сервисной бд
         this.dbService = this.connect.db('service');

         // подключаемся к основной бд
         this.dbDataYGS = this.connect.db('dataYGS');

         // подключаемся к бд с полученными трофеями
         this.dbEarnedTrophies = this.connect.db('earnedTrophies');

         // список коллекций
         this.collections = {
            //коллекция очередь обновления
            updateQueueCollection: this.dbService.collection('updateQueue'),
            // коллекциия с юзерами
            usersCollection: this.dbDataYGS.collection('users'),
            // коллекция с играми юзеров
            userGamesCollection: this.dbDataYGS.collection('userGames'),
            // коллекция с описаниями трофеев
            trophiesCollection: this.dbDataYGS.collection('trophies'),
            // коллекция с основной информацией по играм
            gamesInfoCollection: this.dbDataYGS.collection('gamesInfo'),
            // коллекция для поиска по языкам
            searchRuGame: this.dbDataYGS.collection('gamesInfo_ru'),
            // коллекция с полученными трофеями
            earnedTrophyCollection: this.dbEarnedTrophies.collection('earnedTrophies'),
         };
      } catch (err) {
         console.log('Ошибка при инициализации экземпляра DBManager. Метод: init ==> ', err);
      }
   }

   /** Возвращает список из 10 юзеров для обновления. Сортирует по самому раннему обновлению */
   async getUserListToUpdate(skip, limit){
      try{
         let result = await this.collections.usersCollection.find(
            {}, 
            {
               projection:  { userPsnId: 1, userLogin: 1, },
               sort: {userLastUpdate: -1},
               skip,
               limit,
         }).toArray();
      }catch(err){

      }
   }

   /**
    * записывает n кол-во задач в очередь на обновление
    * @param {object<Array>} listTask - массив объектов которые предстаявляют задачу на обновление
    * @returns 
    */
   async addTaskInQueue(listTask){
      try{
         return await this.collections.updateQueueCollection.insertMany(listTask);
      }catch(err){
         console.log('Ошибка в getTaskUpdate ==> ', err);
      }
   }

   /** Возвращает первую свободную задачу */
   async getTaskUpdate(){
      try{
         // ищет сначала приоритетную задачу и уже потом любую свободную
         return await this.collections.updateQueueCollection.findOne({ $and: [{priority: true}, {work: false}], $or: [{work: false}] }, {projection: {_id:  1, step: 1} });
      }catch(err){
         console.log('Ошибка в getTaskUpdate ==> ', err);
      }
   }

   /** В коллекции updateQueue cтавит work в status для документа idTask
    * @param {String} - idTask - идентификатор документа в коллекции updateQueueCollection
    * @param {Boolean} - status - значение для поля work = true/false
    */
   async setWorkStatus(idTask, status){
      try{
         await this.collections.updateQueueCollection.updateOne({_id: idTask}, {$set: {work: status}});
      }catch(err){
         console.log('Ошибка в setWorkStatus ==> ', err);
      }
   }

   /** Обновляет задчачу. Записывает в поля(setFields)  переданное информацию
    * @param {object} - taskInfo - объект с параметрами для поиска задачи { idTask: string, field: string, setFields: object }
    */
   async updateTaskInfo({idTask, setFields}){
      try{
         await this.collections.updateQueueCollection.updateOne(
            {_id: idTask}, 
            {
               $set: setFields,
               // {
               //    [taskInfo.field]: payload, // объект с данными из psn
               //    work: false,  // свободен для следующего задания
               //    step: taskInfo.step, // метод для следующего вызова
               // }
            }
         );
      }catch(err){
         console.log('Ошибка в setFieldInfo ==> ', err);
      }
   }

   /** Ищет документ в коллекции updateQueue по идентификатору и возвращает поля из списка projection 
    * @param {string} - idTask - строка с идентификаторром документа
    * @param {object} - projection - объект с необходимыми для выборки полями
   */
   async getTaskById(idTask, projection){
      try{
         return await this.collections.updateQueueCollection.findOne({_id: idTask}, {projection});
      }catch(err){
         console.log('Ошибка в getTaskById')
      }
   }

   /** Обновляет профиль игрока  
    * @param {string} - id - строка идентификатора документа в userCollection
    * @param {object} - updateInfo - объект с данными профиля
    * @return {object}
   */
   async updateUserProfile(id, updateInfo){
      try{
         await this.collections.usersCollection.updateOne(
            {_id: id}, 
            {
               $set: updateInfo,
               
                  // {
                  //    userLogin: updateInfo.userLogin,
                  //    userDescription: updateInfo.userDescription,
                  //    userIcons: updateInfo.userIcons,
                  // }
            },
            { upsert: true }
         );
      }catch(err){
         console.log('Ошибка в updateUserProfile ==> ', err);
      }
   }
   /**
    * Удаляет задачу по idTask  из очереди на обновление updateQueueCollection
    * @param {string} idTask - строка идентификатор документа в коллекции updateQueue
    */
   async deleteTaskById(idTask){
      try{
         let result = await this.collections.updateQueueCollection.deleteOne({_id: idTask});
         // TODO возможно после тестов, удалить эту проверку
         // проверяем на удаление
         if(!result.acknowledged){ throw new Error(`Не завершено удаление deleteTaskById по _id: ${idTask}`) }
      }catch(err){
         console.log('Ошибка в deleteTaskById ==> ', err);
      }
   }

   /**
    * Возвращает игры юзера(userId) в кол-ве(limit)
    * @param {string} userId - строка с идентификатором документа юзера
    * @param {number} limit - лимит выборки
    */
   async getUserGames(userId, limit){
      try{
         return await this.collections.userGamesCollection.find(
            {userId}, 
            {
               // TODO это поле нужно заменить, т.к. это поле хранит дату получения последнего трофея для главной игры.
               // а нужно добавить поле которое будет хранить дату последнего полученного трофея для игры(главной) и всех ее dlc 
               projection: {gameMainLastActive: 1},
               sort: {
                  gameMainLastActive: -1
               },
               limit,
            }
            ).toArray();
      }catch(err){
         console.log('Ошибка в getUserGames ==> ', err);
      }
   }

   /**
    * Возвращает игры пользователя по его идентификатору и идентификатору главной игры. 
    * ПРИМЕЧАНИЕ: Хранится главная и как ее поле все dlc этой игры
    * @param {string} userId - строка с идентификатором mongo пользователя
    * @param {string} gameId - строка с идентификаторм mongo главной игры 
    * @returns {object | null}
    */
   async getUserGameById(userId, gameId){
      try{
         return await this.collections.userGamesCollection.findOne(
            {$and: [{_id: userId}, {gameMainId: gameId}]},
            {
               projection: {
                  gameList: 1,
                  gameMainLastActive: 1,
               }
            }
         );
      }catch(err){
         console.log('Ошибка в getUserGameById ==> ', err);
      }
   }
   /**
    * Ищет игру в коллекции(gamesInfo) игр по ее  npCommunicationId
    * @param {object} condition - объект с условием поиска
    * @returns {object | null}
    */
   async getGameInfo(condition){
      try{
         return await this.collections.gamesInfoCollection.findOne(condition);
      }catch(err){
         console.log('Ошибка в getGameInfo ==> ', err);
      }
   }

   /**
    * Возвращает список(массив) dlc по идентификатору основной игры
    * @param {string} mainGameId - строка с идентификатором mongo в коллекции gamesInfo
    */
   async getDLCList(mainGameId){
      try{
         return await this.collections.gamesInfoCollection.find({gameParent: mainGameId}, {projection: {gameNumber: 1}}).toArray();
      }catch(err){
         console.log('Ошибка в getDLCList ==> ', err);
      }
   }

   /**
    * Записывает новыую игру(плюс dlc) в коллекцию userGames
    * @param {object} objUserGame - объект с данными для записи
    * @returns {object}
    */
   async addUserGame(objUserGame){
      try{
         return await this.collections.userGamesCollection.insertOne(mainGame);
      }catch(err){
         console.log('Ошибка в addUserGame ==> ', err);
      }
   }

   /**
    * Обновляев userGames по condition, записывая данные из newData
    * @param {object} condition - объект с условием поиска документа
    * @param {object} newData - объект с новыми данными
    * @returns {objec}
    */
   async updateUserGameById(condition, newData){
      try{
         return await this.collections.userGamesCollection.updateOne(
            {condition},
            {$set: newData}
         );
      }catch(err){
         console.log('Ошибка в updateUserGameById ==> ', err);
      }
   }
   /**
    * TODO удалить потом этот метод
    * Сбрасывает счетчик короткого вычисления на ноль
    * @param {string} idTask - идентификатор задачи
    */
   async dropShort(idTask){
      try{
         await this.collections.updateQueueCollection.updateOne({_id: idTask}, {$set: {short: 0}});
      }catch(err){
         console.log('Ошибка в dropShort ==> ', err);
      }
   }

   /**
    * Увеличивает счетчик короткого вычисления на один
    * @param {string} idTask - идентификатор задачи
    */
   async increaseShort(idTask){
      try{
         await this.collections.updateQueueCollection.updateOne({_id: idTask}, {$inc: {short: 1}});
      }catch(err){
         console.log('Ошибка в increaseShort ==> ', err);
      }
   }

   /**
    * Записывает в коллекцию gamesInfo новую игру
    * @param {object} objMainGame - объект с данными главной игры
    * @returns 
    */
   async addNewGameInGamesInfo(objGame){
      try{
         return await this.collections.gamesInfoCollection.insertOne(objGame);
      }catch(err){
         console.log('Ошибка в addNewGame ==> ', err);
      }
   }

   /**
    * Возвращает список полученных трофеев пользователя по его userId 
    * для игры по gameId
    * @param {string} userId - идентификатор пользователя из коллекции users
    * @param {string} gameId - идентификатор игры из коллекции gamesInfo
    * @returns {object | null}
    */
   async getEarnedTrophies(userId, gameId){
      try{
         return await this.collections.earnedTrophyCollection.findOne({$and: [{getEarnedTrophies: userId}, {earnedGameID: gameId}]});
      }catch(err){
         console.log('Ошибка в getEarnedTrophies ==> ', err);
      }
   }

   /**
    * Обновляет документ с полученными трофеями пользователя, если нет такого документа, то создает
    * @param {string} userId - идентификатор юзера из коллекции users
    * @param {*} gameId - идентификтаоригры из коллекции gamesInfo
    * @param {*} objEarnedTrophies - объект с полученными трофеями, или в сучае если игра PS5 то прогресс получения для трофеев
    * @returns {object} - объект смотрить infoDoc/UpdateResult.txt
    */
   async upsertEarnedTrophies(userId, gameId, objEarnedTrophies){
      try{
         return await this.collections.earnedTrophyCollection.updateOne(
            { $and: [{earnedUserId: userId}, {earnedGameID: gameId}] },
            { $set: objEarnedTrophies },
            { upsert: true }
         );
      }catch(err){
         console.log('Ошибка в upsertEarnedTrophies ==> ', err);
      }
   }

   /**
    * Возвращает все трофеи для игры по ее gameId. Может быть как главная игра, так и dlc
    * возврат содержит:
    * _id - идентификатор трофея
    * trophyId - номер трофея
    * trophyTargetValue - МОЖЕТ ОТСУТСТВОВАТЬ! ТОЛЬКО для PS5! Число необходимых задач для получения трофея
    * @param {string} gameId - идентификатор игры(иди dlc)
    * @returns {object<Array>} - массив трофеев
    */
   async getInfoTrophiesByGameId(gameId){
      try{
         return await this.collections.trophiesCollection.find(
            { trophyGameId: gameId }, 
            { projection: {trophyId: 1, trophyTargetValue: 1} }
         ).toArray();
      }catch(err){
         console.log('Ошибка в getInfoTrophiesByGameId ==> ', err);
      }
   }

   /**
    * Записывает в коллекцию trophies информацию о новом трофеее
    * @param {object} objInfoTrophy - объект с информацией нового трофея
    * @returns {object} - объект сполями acknowledged и insertedId
    */
   async addInfoTrophies(objInfoTrophy){
      try{
         return await this.collections.trophiesCollection.insertOne(objInfoTrophy);
      }catch(err){
         console.log('Ошибка в addInfoTrophies ==> ', err);
      }
   }
}
